// 1 AJAX  технологія, яка дозволяє взаємодіяти з сервером без перезавантаження сторінки

const API = "https://ajax.test-danit.com/api/swapi/films";

fetch(API)
  .then((response) => response.json())
  .then((films) =>
    films.forEach((film) => {
      console.log(film);

      let name = document.createElement("h2");
      let charactersL = document.createElement("p");
      let episodeId = document.createElement("p");

      let openingCrawl = document.createElement("p");

      name.innerHTML = film.name;
      episodeId.innerHTML = film.episodeId;
      openingCrawl.innerHTML = film.openingCrawl;
      charactersL.textContent = "";

      document.body.append(episodeId);
      document.body.append(name);
      document.body.append(openingCrawl);
      document.body.append(charactersL);

      let requests = film.characters.map((url) =>
        fetch(url).then((response) => response.json())
      );

      Promise.all(requests)
        .then((responses) =>
          responses.map((character) => character.name).join(", ")
        )
        .then((charString) => {
          charactersL.textContent = charString;
        });
    })
  );



  