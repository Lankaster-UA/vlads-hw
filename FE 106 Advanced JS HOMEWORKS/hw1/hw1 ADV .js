//1 Прототипне наслідування в JavaScript дозволяє об'єктам наслідувати властивості та методи від інших об'єктів. Кожен об'єкт має властивість [[Prototype]], яка є посиланням на інший об'єкт (прототип). Коли ви звертаєтеся до властивості або методу об'єкта, JavaScript спочатку шукає його в самому об'єкті, а потім, якщо не знаходить, продовжує пошук у прототипі та далі вгору по ланцюжку прототипів. Це дозволяє об'єктам "спадковувати" функціональність без потреби визначати її кожного разу.
//2 Виклик super() у конструкторі класу-нащадка в JavaScript є необхідним, коли ви працюєте з класами, що використовують наслідування

class Employee {
  #name;
  #age;
  #salary;
  constructor(name, age, salary) {
    this.name = name;
    this.age = age;
    this.salary = salary;
  }

  get name() {
    return this.#name;
  }
  set name(value) {
    this.#name = value;
  }
  get age() {
    return this.#age;
  }

  set age(value) {
    if (value >= 16) {
      this.#age = value;
    } else {
      throw new Error("The enter is not allowed!");
    }
  }

  get salary() {
    return this.#salary;
  }
  set salary(value) {
    if (value > 0) {
      this.#salary = value;
    } else {
      throw new Error("The salary shoud be more than 0!");
    }
  }
}

let obj = new Employee("Vladyslav", 20, 1500);
obj.age = 20;

console.log(obj);

class Programmer extends Employee{
  #lang
  constructor(name, age, salary, lang){
    super(name, age, salary);
    this.#lang = lang;
  }
  showSalary() {
    return this.salary*3;
    
  }
  
  get lang() {
    return this.#lang;
  }
  set lang(value) {
    this.#lang = value;
  }
}
let dev1 = new Programmer("Igor", 40, 8000, 'js');
let dev2 = new Programmer("Ivan", 31, 9000, 'c++');

console.log(dev1.showSalary());

