// Конструкцію try...catch доцільно використовувати при роботі з файлами для обробки винятків, таких як відсутність файлу або помилки доступу до нього. Також її корисно використовувати при взаємодії з мережею, для перехоплення помилок з'єднання чи обробки винятків під час виконання мережевих запитів.



document.addEventListener('DOMContentLoaded', () => {
  const books = [
      { author: "Люсі Фолі", name: "Список запрошених", price: 70 }, 
      { author: "Сюзанна Кларк", name: "Джонатан Стрейндж і м-р Норрелл" }, 
      { name: "Дизайн. Книга для недизайнерів.", price: 70 }, 
      { author: "Алан Мур", name: "Неономікон", price: 70 }, 
      { author: "Террі Пратчетт", name: "Рухомі картинки", price: 40 },
      { author: "Анґус Гайленд", name: "Коти в мистецтві" }
  ];

  const root = document.getElementById('root');
  if (!root) return; 

  const ul = document.createElement('ul');
  
  books.forEach(book => {
    try {
    
      if (typeof book.author === 'undefined' || typeof book.name === 'undefined' || typeof book.price === 'undefined') {
        let missingProps = [];
        if (typeof book.author === 'undefined') missingProps.push('author');
        if (typeof book.name === 'undefined') missingProps.push('name');
        if (typeof book.price === 'undefined') missingProps.push('price');
        throw new Error(`Відсутні властивості: ${missingProps.join(', ')}`);
      }

      const li = document.createElement('li');
      li.textContent = `${book.author}: "${book.name}" - ${book.price} грн.`;
      ul.appendChild(li);
    } catch (error) {
      console.error(error.message);
    }
  });

  root.appendChild(ul);
});
