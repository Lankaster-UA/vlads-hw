class Card {
    constructor(post, user, onDelete, onEdit) {
        this.post = post;
        this.user = user;
        this.onDelete = onDelete;
        this.onEdit = onEdit;
    }

    createCard() {
        const cardWrapper = document.createElement("div");
        cardWrapper.classList.add("card");
        cardWrapper.dataset.postId = this.post.id;

        const userInfo = document.createElement("div");
        userInfo.classList.add("user-info");
        userInfo.innerHTML = `
        <h2>${this.user.name} ${this.user.surname}</h2>
        <p>Email: ${this.user.email}</p>`;

        const postInfo = document.createElement("div");
        postInfo.classList.add("post-info");
        postInfo.innerHTML = `
        <h3>${this.post.title}</h3>
        <p>${this.post.body}</p>`;

        const deleteButton = document.createElement("button");
        deleteButton.textContent = "Delete";
        deleteButton.classList.add("delete-btn");
        deleteButton.addEventListener("click", () => this.deletePost());

        const editButton = document.createElement("button");
        editButton.textContent = "Edit";
        editButton.classList.add("edit-btn");
        editButton.addEventListener("click", () => this.editPost());

        cardWrapper.appendChild(userInfo);
        cardWrapper.appendChild(postInfo);
        cardWrapper.appendChild(deleteButton);
        cardWrapper.appendChild(editButton);

        document.getElementById("posts-container").prepend(cardWrapper);
    }

    deletePost() {
        fetch(`https://ajax.test-danit.com/api/json/posts/${this.post.id}`, {
            method: "DELETE",
        })
            .then((response) => {
                if (response.ok) {
                    this.onDelete(this.post.id);
                } else {
                    console.error("Failed to delete post:", response.statusText);
                }
            })
            .catch((error) => {
                console.error("Error deleting post:", error);
            });
    }

    editPost() {
        const newTitle = prompt("Enter new title:", this.post.title);
        const newBody = prompt("Enter new body:", this.post.body);
        if (newTitle !== null && newBody !== null) {
            const updatedPost = { ...this.post, title: newTitle, body: newBody };
            this.onEdit(updatedPost);
        }
    }
}

document.addEventListener("DOMContentLoaded", () => {
    const loadingAnimation = document.getElementById("loading-animation");
    loadingAnimation.innerHTML = "Loading...";

    fetch("https://ajax.test-danit.com/api/json/users")
        .then((response) => response.json())
        .then((users) => {
            fetch("https://ajax.test-danit.com/api/json/posts")
                .then((response) => response.json())
                .then((posts) => {
                    loadingAnimation.innerHTML = "";
                    displayPosts(posts, users);
                })
                .catch((error) => {
                    console.error("Error fetching posts:", error);
                    loadingAnimation.innerHTML = "Error fetching posts";
                });
        })
        .catch((error) => {
            console.error("Error fetching users:", error);
            loadingAnimation.innerHTML = "Error fetching users";
        });
});

function displayPosts(posts, users) {
    posts.forEach((post) => {
        const user = users.find((user) => user.id === post.userId);
        if (user) {
            const card = new Card(post, user, deletePost, editPost);
            card.createCard();
        } else {
            console.error("User not found for post:", post);
        }
    });
}

function deletePost(postId) {
    const cardToDelete = document.querySelector(
        `.card[data-post-id="${postId}"]`
    );
    if (cardToDelete) {
        cardToDelete.remove();
    } else {
        console.error("Card not found for post:", postId);
    }
}

function editPost(updatedPost) {
    const cardToUpdate = document.querySelector(
        `.card[data-post-id="${updatedPost.id}"]`
    );
    if (cardToUpdate) {
        const postInfo = cardToUpdate.querySelector(".post-info");
        postInfo.innerHTML = `
        <h3>${updatedPost.title}</h3>
        <p>${updatedPost.body}</p>
      `;
    } else {
        console.error("Card not found for post:", updatedPost.id);
    }
}

const addPostBtn = document.getElementById("add-post-btn");
addPostBtn.addEventListener("click", () => {
    document.getElementById("modal").style.display = "block";
});

const closeModalBtn = document.querySelector(".close");
closeModalBtn.addEventListener("click", () => {
    document.getElementById("modal").style.display = "none";
});

const submitPostBtn = document.getElementById("submit-post-btn");
submitPostBtn.addEventListener("click", () => {
    const title = document.getElementById("post-title").value.trim();
    const body = document.getElementById("post-body").value.trim();
    if (title && body) {
        createNewPost(title, body);
        document.getElementById("modal").style.display = "none";
    } else {
        alert("Please enter both title and body for the post.");
    }
});

function createNewPost(title, body) {
    const newPost = {
        title: title,
        body: body,
        userId: 1,
    };

    fetch("https://ajax.test-danit.com/api/json/posts", {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(newPost),
    })
        .then((response) => response.json())
        .then((data) => {
            console.log("New post created:", data);
            const users = [
                { id: 1, name: "New", surname: "User", email: "newuser@example.com" },
            ]; // Hardcoded user data
            const card = new Card(data, users[0], deletePost, editPost);
            card.createCard();
        })
        .catch((error) => {
            console.error("Error creating new post:", error);
            alert("Error creating new post. Please try again later.");
        });
}
