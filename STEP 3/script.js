class Modal{
    constructor(title, content, callback){
        this.title = title;
        this.content = content;
        this.callback = callback;
    }
    render(){
document.body.insertAdjacentHTML('afterbegin', 
   ` <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title fs-5" id="exampleModalLabel">${this.title}</h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
       ${this.content}
       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="button" onclick="${this.callback()}" id="callback" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>`
)
let btnCallback = document.getElementById("callback");
btnCallback.onclick = this.callback;
let creatVisit = document.getElementById("creatVisit")
if (creatVisit) {
  let select = creatVisit.querySelector("select");
  let addFields = creatVisit.querySelector(".addFields");
 
  select.onchange = (event)=>{

    let visit ;
if (event.target.value === "cardiologist"){
   visit = new Visit({});
}
    //зробити по аналогії за допомогою дочірних класів
    addFields.innerHTML = visit ? visit.renderFileds() : "";
  };
}
    }
}

const form = ` <form id="creatVisit">
<select class="form-select form-select-lg mb-3" aria-label="Large select example">
    <option selected>Оберіть лікаря</option>
    <option value="cardiologist">Кардіолог</option>
    <option value="dentist">Стоматолог</option>
    <option value="therapist">Терапевт</option>
  </select>
<div class="addFields">


</div>

</form>
`;

const newModal = new Modal("Створити візит", form, ()=>{
    console.log(newModal);
});
newModal.render();

class Visit{
    constructor({purpose = "", description = "", doctor = "", fullName = "", id = ""}){
        this.purpose = purpose;
        this.description = description;
        this.doctor = doctor;
        this.fullName= fullName;
        this.id = id;
    }
    renderFileds(){
       return ` <div class="mb-3">
        <label for="purpose" class="form-label">purpose</label>
        <input type="text" name ="purpose" class="form-control" id="purpose" aria-describedby="emailHelp">
       
      </div>
      
      <div class="mb-3">
        <label for="description" class="form-label">description</label>
        <input type="text" name ="description" class="form-control" id="description" aria-describedby="emailHelp">
       
      </div>
      
      <div class="mb-3">
      <label for="doctor" class="form-label">doctor</label>
      <input type="text" name ="doctor" class="form-control" id="doctor" aria-describedby="emailHelp">
     
    </div>
    
    <div class="mb-3">
    <label for="fullName" class="form-label">Full Name</label>
    <input type="text" name ="fullName" class="form-control" id="fullName" aria-describedby="emailHelp">
   
  </div>`
    }
}


// додадти сюди евент лісенер
// як робити наслувадння
// {/* <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div> */}