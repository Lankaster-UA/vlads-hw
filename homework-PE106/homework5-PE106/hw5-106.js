// 1 Метод об'єкту -  метод об'єкту - це дія, яку можна виконати над конкретним об'єктом даного класу. Методи дозволяють об'єктам взаємодіяти між собою та виконувати різні операції з об'єктом або його властивостями.

// 2 Типи даних властивостей можуть включати цілі числа, рядки, булеві значення, масиви, інші об'єкти або навіть вказівники на функції. 

// 3 Об'єкт являє собою посилальний тип даних означає він не зберігає самі дані, але вказує на місцезнаходження даних в пам'яті.

function createNewUser(){
    let userFirstName = prompt('What is your first name?');
    let userLastName = prompt('What is your last name?');
    let newUser ={
        userName : userFirstName,
        userLastName : userLastName
    };
    newUser.getLogin = function() {
        return this.userName[0].toLocaleLowerCase() + this.userLastName.toLocaleLowerCase();
        
    };
    return newUser;
};

const createUser = createNewUser(); 

console.log(createUser.getLogin());
