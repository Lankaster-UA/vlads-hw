import gulp from 'gulp';
const { src, dest, watch, series, parallel } = gulp;

import autoprefixer from 'gulp-autoprefixer';
import clean from 'gulp-clean';
import csso from 'gulp-csso';
import imagemin from 'gulp-imagemin';
import gulpSass from 'gulp-sass';
import dartSass from 'sass';
const sass = gulpSass(dartSass);

import bsc from 'browser-sync';
const browserSync = bsc.create();

const htmlTaskHandler = () => {
  return src('./src/*.html').pipe(dest('./dist'));
};
const buildJs = () => gulp.src('./src/js/*.js').pipe(gulp.dest('./dist/js')).pipe(browserSync.stream());
const cssTaskHandler = () => {
  return src('./src/scss/main.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer())
    .pipe(csso())
    .pipe(dest('./dist/css'))
    .pipe(browserSync.stream());
};

const imagesTaskHandler = () => {
  return src('./src/images/**/*.*').pipe(imagemin()).pipe(dest('./dist/images'));
};

const fontTaskHandler = () => {
  return src('./src/fonts/**/*.*').pipe(dest('./dist/fonts'));
};

const cleanDistTaskHandler = () => {
  return src('./dist', { read: false, allowEmpty: true }).pipe(clean({ force: true }));
};

const browserSyncTaskHandler = () => {
  browserSync.init({
    server: {
      baseDir: './dist',
    },
  });

  watch('./src/scss/**/*.scss').on('all', series(cssTaskHandler, browserSync.reload));
  watch('./src/*.html').on('change', series(htmlTaskHandler, browserSync.reload));
  watch('./src/images/**/*').on('all', series(imagesTaskHandler, browserSync.reload));
};

export const cleaning = cleanDistTaskHandler;
export const html = htmlTaskHandler;
export const css = cssTaskHandler;
export const font = fontTaskHandler;
export const images = imagesTaskHandler;
export const js = buildJs;

export const build = series(cleanDistTaskHandler, parallel(htmlTaskHandler, cssTaskHandler, fontTaskHandler, js, imagesTaskHandler));
export const dev = series(build, browserSyncTaskHandler);
