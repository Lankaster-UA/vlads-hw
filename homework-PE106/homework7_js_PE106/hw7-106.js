// 1. forEach - використовується для виконання заданої функції один раз для кожного елемента у масиві. Він приймає callback функцію , яка викликається для кожного елемента, і ця функція може виконати певні дії або операції для кожного елемента масиву. При цьому сам масив залишається незмінним.
// 2. присвоїти йому порожній масив. Ось приклад:
//const myArray = [1, 2, 3, 4];
//myArray.length = 0;
//Щоб перевірити, чи змінна є масивом в JavaScript, можна використати метод Array.isArray()



function filterBy(arr, type) {
    if(type === 'null'){
        let NewArr = arr.filter(element => element !== null);
        return NewArr;
    } else {
    let NewArr = arr.filter(element => typeof element !== type);
    return NewArr;
}};

let array = ['hello', 'world', 23, '23', null, true, false, {}, [], function(){}];

console.log(filterBy(array, 'null')); 

