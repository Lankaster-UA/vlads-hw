//1 DOM —   модель документа, яка представляє веб-сторінку як дерево об'єктів. 

// 2 innerHTML повертає весь вміст елемента, включаючи HTML-теги та атрибути.
// innerText повертає лише текстовий вміст елемента, без HTML-тегів.

// 3 getElementById, querySelector, querySelectorAll. Думаю querySelector найгнучкіший. 

const paragraphs = document.querySelectorAll("p");
paragraphs.forEach(paragraph => {
    paragraph.style.backgroundColor = "#ff0000";
  });
console.log(paragraphs);

const optionsList = document.getElementById('optionsList');

console.log(optionsList);
console.log(optionsList.parentElement);

optionsList.childNodes.forEach((element) => console.log(element.nodeName, element.nodeType));


const testParagraph = document.getElementById('testParagraph');

testParagraph.innerText = 'This is a paragraph';

console.log(testParagraph.innerText);

const elemMainHeader = document.querySelector('.main-header');
for (let index = 0; index < elemMainHeader.children.length; index++) {
  const element = elemMainHeader.children[index];
  console.log(element);
  element.classList.add("nav-item");
};

const sectionTitle = document.querySelectorAll('.section-title');
sectionTitle.forEach(element => {
  element.classList.remove('section-title');
  console.log(element);
});




