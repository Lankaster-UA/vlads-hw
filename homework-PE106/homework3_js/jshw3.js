// 1 У JS цикли  дозволяють виконувати однаковий код кілька разів без необхідності повторювати його кожен раз вручну. Це полегшує роботу з великою кількістю даних або виконання однотипних завдань.
// 2 У JavaScript є два основних види циклів: цикл "for" та цикл "while". Цикл "for" зазвичай використовується для ітерації (перебору) крізь певний діапазон або колекцію даних, в той час як цикл "while" використовується для виконання блоку коду, доки певна умова залишається істинною.

// 3 Явне  перетворення даних у JavaScript - це зміна типу даних, яку розробник виконує свідомо за допомогою вбудованих функцій.
// Неявне приведення типів даних - автоматична зміна типу даних, яка відбувається автоматично при виконанні операцій над даними різних типів в JavaScript.

//основен завадання 

// let userNum = +prompt("Enter your number!");
//     while (!Number.isInteger(userNum)) {
//         userNum = +prompt("Enter corect number!");
//     }
// if (!userNum || userNum < 0 ) {
//     alert("Sorry, no numbers");
// }else {
// for (let i = 0; i <= userNum; i++) {
//   if(i % 5 === 0){     
//     console.log(i)} }
// };


//Необов'язкове завдання підвищеної складності
let mNum = +prompt("Enter m number!");
let nNum = +prompt("Enter n number!");

while (mNum > nNum) {
    alert('Error!');
     mNum = +prompt("Enter m number again!");
     nNum = +prompt("Enter n number again!");
}

for (let index = mNum; index <= nNum; index++) {
    if(isNumSimp(index)){
        console.log(index);
    }
};


function isNumSimp(number){
if(number < 2){
return false;
};
for (let index = 2; index <= Math.sqrt(number); index++) {
    if (number % index === 0) {
        return false;
    }
} return true;
};
