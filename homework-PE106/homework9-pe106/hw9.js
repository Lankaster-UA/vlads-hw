function getArr(arr, def = document.body) {
    const ul = document.createElement('ul');
    def.prepend(ul);

    arr.forEach(element => {
        const li  = document.createElement('li');
        li.innerText = element;
        ul.append(li);
    });
};

const arr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];

getArr(arr);

