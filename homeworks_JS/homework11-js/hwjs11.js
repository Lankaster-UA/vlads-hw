const passwordForm = document.querySelector(".password-form");

passwordForm.addEventListener("click", function (event) {
  let curentElement = event.target;
  if (curentElement.tagName === "I") {
    let inputWrapper = curentElement.closest(".input-wrapper");
    let activeInput = inputWrapper.querySelector("input");
    if (curentElement.classList.contains("fa-eye-slash")) {
      curentElement.classList.add("fa-eye");
      curentElement.classList.remove("fa-eye-slash");
      activeInput.type = "password";
    } else {
      curentElement.classList.remove("fa-eye");
      curentElement.classList.add("fa-eye-slash");
      activeInput.type = "text";

      console.log("I");
    }
  }
  if (curentElement.tagName === "BUTTON") {
    event.preventDefault();
    let passwordInput = document.querySelector("#password-input");
    let passwordConfirmInput = document.querySelector(
      "#password-confirm-input"
    );
    if (passwordInput.value === passwordConfirmInput.value) {
      alert("You are welcome");
    } else {
      let errorMessege = document.querySelector(".error-messege");
      errorMessege.innerHTML =
        '<p style= "color: red;"> Потрібно ввести однакові значення</p>';
    }
  }
});
