//1. Екранування потріно щоб вставляти такі символи як наприклад "'" і т.д.
//2. За допомогою слова-ключа Function оголошується функції як вираз і стрілкові функції.
//3. Hoisting - це механізм в JS, який дозволяє оголошення функцій та змінних перед їх фактичним оголошенням у коді, але змінні не отримують значення до моменту їх ініціалізації.

function createNewUser() {
  let firstName = prompt("What is your name?").trim();
  let lastName = prompt("What is your lastname?").trim();
  let birthDate = prompt("When is birth day (dd.mm.yyyy)?").trim();
  let newUser = {
    firstName,
    lastName,
    birthDate,
    getLogin() {
      return (this.firstName[0] + lastName).toLowerCase();
    },
    getAge() {
      let today = new Date();
      let splitDate = this.birthDate.split(".");
      let date = +splitDate[0].trim();
      let month = +splitDate[1].trim();
      let year = +splitDate[2].trim();
      let newDay = new Date(year, month - 1, date);
      let age = today.getFullYear() - newDay.getFullYear();
      let m = today.getMonth() - newDay.getMonth();
      if (m < 0 || (m === 0 && today.getDate() < newDay.getDate())) {
        age--;
      }
      return `Your age: ${age}`;
    },
    getPassword() {
      let year = this.birthDate.split(".")[2].trim();
      return `${this.firstName[0].toUpperCase()}${this.lastName.toLowerCase()}${year}`;
    },
  };
  return newUser;
}

let user = createNewUser();
console.log(user.firstName, user.lastName);
console.log(user.getLogin());
console.log(user.getAge());
console.log(user.getPassword());

