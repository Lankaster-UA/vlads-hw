//1. Метод об'єкту - це функція, яка пов'язана з об'єктом і виконується в середені цього об'єкту. 
//2. Значення властивості об'єкта може мати будь-який тип даних: примітивні (числа, рядки, логічні значення )  або складні (масиви, об'єкти, функції).
//3. Об'єкт є посилальним типом даних, що означає, що зміна об'єкту в одному місці може відобразитися на його значеннях по всій программі. 

function createNewUser(){
    let firstName = prompt ("What is your name?");
    let lastName = prompt ("What is your lastname?");
    let newUser = {firstName, lastName, getLogin(){return this.firstName[0].toLowerCase() + lastName.toLowerCase()}};
    return newUser
} 
let user = createNewUser();
console.log(user.firstName,user.lastName);
console.log(user.getLogin())