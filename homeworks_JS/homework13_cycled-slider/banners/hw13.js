//Основна різниця між ними полягає в тому, скільки разів вони виконуються. setTimeout() виконує код один раз після вказаного проміжку часу, а setInterval() виконує код повторно через вказаний проміжок часу.
//Функція все одно буде викликана тільки після того, як браузер обробить всі поточні завдання. Це пов'язано з тим, що JavaScript є однопотоковою мовою програмування, тому всі функції виконуються в одному потоці.
//Важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл вам вже не потрібен, щоб зменшити навантаження на ресурси і підтримувати оптимальну продуктивність.

const images = document.querySelectorAll('.image-to-show');
let currentIndex = 0;
let showImage;

function swapActive(){
     showImage = setInterval(function timer(){
      images[currentIndex].classList.remove("active");
      console.log(currentIndex);
      currentIndex = (currentIndex + 1) % images.length;
      images[currentIndex].classList.add("active");
      console.log(currentIndex);
    }, 3000);
}
swapActive();
console.log(currentIndex);

let btnStop = document.querySelector("#btn-stop");
let btnCon = document.querySelector("#btn-con");
btnCon.disabled = true;

btnStop.addEventListener('click', function(){
  clearInterval(showImage);
  btnCon.disabled = false;
});
btnCon.addEventListener('click', function(){
  clearInterval(showImage);
  swapActive();
  btnCon.disabled = true;
});
