// Дудмаю головне це те що вони будуть не зручні для користувачів з моб.телефонів, планшетів.

const buttons = {
  13: document.querySelector(".btn:nth-of-type(1)"), 
  83: document.querySelector(".btn:nth-of-type(2)"), 
  69: document.querySelector(".btn:nth-of-type(3)"), 
  79: document.querySelector(".btn:nth-of-type(4)"), 
  78: document.querySelector(".btn:nth-of-type(5)"), 
  76: document.querySelector(".btn:nth-of-type(6)"), 
  90: document.querySelector(".btn:nth-of-type(7)"), 
};

function changeButtonColor(code) {
if (code in buttons) {
    Object.values(buttons).forEach((button) => {
    if (button.style.backgroundColor === "blue") {
        button.style.backgroundColor = "black";
    }
    });
    buttons[code].style.backgroundColor = "blue";
}
}

document.addEventListener("keydown", (event) => {
const code = event.keyCode;
changeButtonColor(code);
});
