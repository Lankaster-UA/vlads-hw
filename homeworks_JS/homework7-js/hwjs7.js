// 1 Метод forEach є вбудованою функцією. Він призначений для перебору елементів масиву або колекції та виконання певної функції  для кожного елемента.
// 2 Найпростіший спосіб очистити масив - присвоїти йому порожній масив [].
// 3 Використання Array.isArray():
// Метод Array.isArray() перевіряє, чи є передане значення масивом і повертає true, якщо це так, або false, якщо це не масив.


function filterBy(arr, type){
     let newArrey = [];
     newArrey = arr.filter(e => typeof e !== type);
     return newArrey;
};

const arr = ['hello', 'world', 23, '23', null];
console.log(filterBy(arr, "number"));