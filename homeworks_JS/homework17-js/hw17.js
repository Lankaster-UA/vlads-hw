let studentName = prompt("What is your name?");
let studentLastName = prompt("What is your lastName?");

let student = {
  name: studentName,
  lastName: studentLastName,
};

console.log(student);

let studentTable = {};

while (true) {
  let subject = prompt("Введіть назву предмета:");

  if (subject === null) {
    break;
  }

  while (subject.trim() === "") {
    subject = prompt("Введіть назву предмета:");
  }

  let grade = +prompt("Введіть оцінку:");
  while (!grade || isNaN(grade)) {
    grade = +prompt("Введіть оцінку:");
  }
  studentTable[subject] = parseFloat(grade);
}

let countBadMarks = 0;
let counterGrades = 0;
let sumGrades = 0;

for (const key in studentTable) {
  if (studentTable[key] < 4) {
    countBadMarks++;
  }
  sumGrades = +sumGrades + studentTable[key];
  counterGrades++;
}
if (countBadMarks === 0 && counterGrades > 0) {
  alert("Студент переведено на наступний курс");
}

let everegGrade = sumGrades / counterGrades;

console.log(sumGrades);
console.log(counterGrades);
console.log(everegGrade);
if (everegGrade > 7) {
  alert("Студенту призначено стипендію");
}