1//document.createElement() і в дужках вказати назву елменту який хочемо створити
2// insertAdjacentHTML який дозволяє вставляти HTML-код в певному місці на сторінці, використовуючи різні позиції відносно елемента, до якого буде здійснено вставку.
// 'beforebegin' - вставити HTML-код перед елементом, до якого прив'язан метод;
// 'afterbegin' - вставити HTML-код в середину елемента, до його першого дочірнього елемента;
// 'beforeend' - вставити HTML-код в середину елемента, після всіх дочірніх елементів;
// 'afterend' - вставити HTML-код після елемента, до якого прив'язан метод.
3// Для видалення елемента  зі сторінки за допомогою JavaScript можна скористатися методом remove().

// function inputArrs(myArray, parent = document.body){
// let list = document.createElement("ul");
// parent.append(list);
// for(let i = 0; i < myArray.length; i++){
//     console.log(myArray[i]);
//     let li = document.createElement("li");
//     li.innerText = myArray[i];
//     list.append(li); 
// }
// }
// const myArray = ["Kherson", "Chernigiv", "Kyiv", "Kharkiv", "Odesa", "Lviv"]
// console.log(myArray);
// inputArrs(myArray);

function inputArrs(myArray, parent = document.body){
    let list = document.createElement("ul");
    parent.append(list);
    const domElementArray = 
    myArray.map((inputArrs, i) => {
        console.log(inputArrs, i);
        let li = document.createElement("li");
        li.innerText = inputArrs;
        return li;
    });
    list.append(...domElementArray);
    }
    const myArray = ["Kherson", "Chernigiv", "Kyiv", "Kharkiv", "Odesa", "Lviv"]
    // console.log(myArray);
    inputArrs(myArray);