const obj = {
    name: "John Doe",
    age: 30,
    address: {
      city: "Kyiv",
      country: "Ukraine",
    },
    hobbies: ["programming", "reading", "traveling"],
  };
  
  const clone = deepClone(obj);
  
  console.log(clone);


function deepClone(obj) {
    if (typeof obj !== "object") {
      return obj;
    }
  
    const clone = {};
    for (const key in obj) {
      const value = obj[key];
      if (typeof value === "object") {
        clone[key] = deepClone(value);
      } else if (Array.isArray(value)) {
        clone[key] = value.map(deepClone);
      } else {
        clone[key] = value;
      }
    }
  
    return clone;
  }
  
